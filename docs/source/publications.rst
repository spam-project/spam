######################
List of publications
######################

This is a list with all the (known) publications that used spam.

.. This is a template - Use APA for copying Authors list and Journal data

    .. dropdown:: YEAR - AUTHOR et al., JOURNAL

        **Title:**

        **Authors:**

        **Journal:**

        **Link:** `Link <>`_

        **Tags:** `spamFunction <linkToFunction>`_,

        **Abstract:**

        HERE GOES THE ABSTRACT

        .. figure:: images/publications/NICEIMAGE.png
                    :align: center




.. dropdown:: 2023 - Pinzón, G., [Doctoral dissertation]

    **Title:** Experimental investigation of the effects of particle shape and friction on the mechanics of granular media

    **Authors:** Pinzón, G.

    **University:** Université Grenoble Alpes

    **Link:** `Link <https://www.theses.fr/2023GRALI015>`_

    **Tags:** `Discrete DIC <https://spam-project.gitlab.io/spam/tutorials/tutorial-04-discreteDIC.html>`_, `Fabric analysis <https://spam-project.gitlab.io/spam/tutorials/tutorial-08-contacts.html#tutorial-contacts-calculation>`_, `Particle segmentation <https://spam-project.gitlab.io/spam/tutorials/tutorial-03-labelToolkit.html#watershed>`_.

.. dropdown:: 2023 - Pinzón, G. et al., Granular Matter

    **Title:** Fabric evolution and strain localisation in inherently anisotropic specimens of anisometric particles (lentils) under triaxial compression.

    **Authors:** Pinzón, G., Andò, E., Desrues, J., & Viggiani, G.

    **Journal:** Granular Matter, 25(1), 15.

    **Link:** `Link <https://link.springer.com/article/10.1007/s10035-022-01305-8>`_

    **Tags:**  `Discrete DIC <https://spam-project.gitlab.io/spam/tutorials/tutorial-04-discreteDIC.html>`_, `Fabric analysis <https://spam-project.gitlab.io/spam/tutorials/tutorial-08-contacts.html#tutorial-contacts-calculation>`_, `Particle segmentation <https://spam-project.gitlab.io/spam/tutorials/tutorial-03-labelToolkit.html#watershed>`_.

    **Abstract:**

    Inherent anisotropy is often observed in natural soils and other granular materials formed during gravity deposition. Fabric characterisation and evolution have been mainly studied using numerical simulations, given the difficulty of retrieving the contact network and the kinematics of particles in physical experiments. This work presents the results of five triaxial compression tests on inherently anisotropic lentil specimens imaged with x-ray tomography. Each specimen is prepared in a way such that all the particles present a unique mean orientation, as results from gravity in natural soils deposits - this can be called “bedding effect”. Particles are identified and tracked from the first image all the way through the test using a novel tracking algorithm, enabling the measurement of particle and contact fabric evolution, as well as strain localisation within the specimens. The results reveal that the deformation processes take place essentially in a planar scenario, both at a micro and mesoscale. Additionally, it is observed that under deviatoric loading two mechanisms are responsible for fabric evolution: rotation of the main orientation of the fabric tensor, and increase of the anisotropy. Finally, the orientation of the shear band is found to be independent of the initial orientation of the particles.

    .. figure:: ./images/publications/pinzon2023.png
                    :align: center

.. dropdown:: 2022 - Wang, R. et al., Journal of Engineering Mechanics

    **Title:** Modeling combined fabric evolution in an anisometric granular material driven by particle-scale X-ray measurements.

    **Authors:** Wang, R., Pinzón, G., Andò, E., & Viggiani, G.

    **Journal:** Journal of Engineering Mechanics, 148(1), 04021120.

    **Link:** `Link <https://ascelibrary.org/doi/abs/10.1061/%28ASCE%29EM.1943-7889.0002032#abstract>`_

    **Tags:**  `Discrete DIC <https://spam-project.gitlab.io/spam/tutorials/tutorial-04-discreteDIC.html>`_, `Fabric analysis <https://spam-project.gitlab.io/spam/tutorials/tutorial-08-contacts.html#tutorial-contacts-calculation>`_, `Particle segmentation <https://spam-project.gitlab.io/spam/tutorials/tutorial-03-labelToolkit.html#watershed>`_.

    **Abstract:**

    A combined fabric evolution (CFE) model is used to predict real-world fabric evolution of a strongly anisometric granular material under triaxial loading, connecting advances in theoretical developments and experimental measurement technology for fabric evolution. X-ray tomography is used to quantify particle orientation and contact normal fabric evolution in five triaxial compression experiments on lentil specimens of different initial bedding plane angles. The CFE model coupling contact normal fabric evolution with particle orientation fabric is calibrated based on two of the experiments and used to predict the fabric evolution in the others. Good overall agreement between theoretical prediction and experimental measurement is achieved for the evolution of both types of fabric tensors. The comparison between prediction and measurement highlights an optimistic future for the development of constitutive relations incorporating fabric features based on actual experimental micromechanical observations. Nonetheless, the special case of 90° deposition is relatively poorly predicted due to the strongly heterogeneous local dilation caused by the extreme particle shape and alignment. This suggests that there is still more to be considered in the continuum description of the fabric evolution of granular materials, especially with respect to local information.

    .. figure:: images/publications/wang2022.png
                    :align: center

.. dropdown:: 2021 - Anselmucci, F. et al., Géotechnique Letters

    **Title:** Use of X-ray tomography to investigate soil deformation around growing roots.

    **Authors:** Anselmucci, F., Andó, E., Viggiani, G., Lenoir, N., Peyroux, R., Arson, C., & Sibille, L

    **Journal:** Géotechnique Letters, 11(1), 96-102.

    **Link:** `Link <https://www.icevirtuallibrary.com/doi/abs/10.1680/jgele.20.00114>`_

    **Tags:**

    **Abstract:**

    The increased shear strength of rooted soil recently inspired researchers to assess the stability of vegetated slopes with continuum models and to design anchors by mimicking root system architectures. Yet, there is no clear understanding to date of why roots affect soil properties. This paper presents an experimental proof of concept based on x-ray tomography that addresses this issue by assessing soil microstructural changes induced by root growth. A three-dimensional time series of the root–soil interaction was imaged for 7 days. The computed local strain tensor highlights that the soil was sheared in the vicinity of the root system. Additionally, the initial bulk density of the soil was found to significantly influence the response of soil to plant root growth. In the sheared zone, the looser soil exhibited a slightly contractant behaviour, while the denser soil was purely dilatant. Further from the root system, no significant volume changes were measured for the denser specimen, whereas compaction was noted in the looser specimen. In contrast with earlier studies, results suggest that the high soil porosity near the root may result not only from steric exclusion, but also from the constitutive soil response to shear deformation.

.. dropdown:: 2021 - Pinzón, G., et al., EPJ Web of Conferences

        **Title:** Contact evolution in granular materials with inherently anisotropic fabric.

        **Authors:** Pinzón, G., Andò, E., Tengattini, A., Viggiani, G., & Desrues, J.

        **Journal:** EPJ Web of Conferences (Vol. 249, p. 06015). EDP Sciences.

        **Link:** `Link <https://www.epj-conferences.org/articles/epjconf/abs/2021/03/epjconf_pg2021_06015/epjconf_pg2021_06015.html>`_

        **Tags:** `Discrete DIC <https://spam-project.gitlab.io/spam/tutorials/tutorial-04-discreteDIC.html>`_

        **Abstract:**

        This paper presents the results of two triaxial compression tests performed on approximately 9×103 oblate spheroids (lentils) with different initial orientations with respect to the loading axis (approximately 30° and 60°). Typical stress-strain information is complemented with x-ray tomography scans every 1 % strain. Starting from an initial labelling of particles, a new technique is used to obtain unprecedented detail of tracking of all the particles through time. This rich dataset is analysed from the perspective of inter-particle contacts (building on previous metrological work on this subject) revealing that the mean contact orientation in both samples rotates towards the direction of σ1 at a rate depending on the initial orientation. Different trends are observed for the alignment of contact orientation, with a significant evolution observed in the sample prepared at 30° which is not as pronounced as in the other sample.

        .. figure:: images/publications/pinzon2021.png
                    :align: center


.. dropdown:: 2021 - Zhao, C. et al., Computers and Geotechnics

    **Title:** Evolution of fabric anisotropy of granular soils: x-ray tomography measurements and theoretical modelling

    **Authors:** Zhao, C. F., Pinzón, G., Wiebicke, M., Andò, E., Kruyt, N. P., & Viggiani, G

    **Journal:** Computers and Geotechnics, 133, 104046.

    **Link:** `Link <https://www.sciencedirect.com/science/article/pii/S0266352X21000495?casa_token=gVN4jzLNsBsAAAAA:JftC-StImBcxBSYuB3LZSMG9kxrkFQ8LDzenVcfziAYriqIzIFJ1jpMp1bAqjQMk7LEm8vPoWw>`_

    **Tags:** `Discrete DIC <https://spam-project.gitlab.io/spam/tutorials/tutorial-04-discreteDIC.html>`_, `Fabric analysis <https://spam-project.gitlab.io/spam/tutorials/tutorial-08-contacts.html#tutorial-contacts-calculation>`_, `Particle segmentation <https://spam-project.gitlab.io/spam/tutorials/tutorial-03-labelToolkit.html#watershed>`_.

    **Abstract:**

    Fabric anisotropy is a key component to understand the behaviour of granular soils. In general, experimental data on fabric anisotropy for real granular soils are very limited, especially in the critical state. In this paper, x-ray tomography measurements are used to provide experimental data on contact fabric anisotropy inside shear bands for two granular soils. The data are then used to assess the validity of Anisotropic Critical State Theory (ACST) and the accuracy of a fabric evolution law that was previously developed from the results of DEM simulations on idealised materials. Overall, the experimental results support ACST according to which unique (i.e., independent of initial conditions) values for fabric anisotropy and coordination number are observed at large strains. With increasing roundness of the material, the rate at which the critical state is approached increases. The evolution of fabric anisotropy measured from the experiments is fairly well reproduced by the proposed evolution law.

    .. figure:: images/publications/zhao2021.png
                    :align: center

.. dropdown:: 2020 - Wiebicke, M., [Doctoral dissertation]

    **Title:** Experimental analysis of the evolution of fabric in granular soils upon monotonic loading and load reversals

    **Authors:** Wiebicke, M.

    **University:** Technischen Universität Dresden & Université Grenoble Alpes

    **Link:** `Link <https://tu-dresden.de/bu/bauingenieurwesen/ressourcen/dateien/postgraduales/pdf/Wiebicke_summary.pdf?lang=en>`_

    **Tags:** `Fabric analysis <https://spam-project.gitlab.io/spam/tutorials/tutorial-08-contacts.html#tutorial-contacts-calculation>`_, `Particle segmentation <https://spam-project.gitlab.io/spam/tutorials/tutorial-03-labelToolkit.html#watershed>`_.

.. dropdown:: 2019 - Dijkstra, J., et al., E3S Web of Conferences

        **Title:** Grain kinematics during stress relaxation in sand: not a problem for X-ray imaging.

        **Authors:** Dijkstra, J., Andò, E., & Dano, C.

        **Journal:** E3S Web of Conferences (Vol. 92, p. 01001). EDP Sciences.

        **Link:** `Link <https://www.e3s-conferences.org/articles/e3sconf/abs/2019/18/e3sconf_isg2019_01001/e3sconf_isg2019_01001.html>`_

        **Tags:** `Discrete DIC <https://spam-project.gitlab.io/spam/tutorials/tutorial-04-discreteDIC.html>`_

        **Abstract:**

        X-ray tomography is a very valuable tool for studying the full-field 3D deformation of granular materials. The requirement to stop loading and scan a given state (assumed to be stationary) used in most approaches implies unavoidable stress relaxation during scanning. Since scanning times on laboratory tomographs are normally in the order of 1 hour, the strength of the assumption of a stationary state cannot be tested, which introduces some potential weakness in the interpretation of the rich micro-mechanics observed. This paper presents the kinematics of relaxation of a dry natural sand in a typical oedometric cell used for X-ray scanning, using a synchrotron X-ray source to provide scanning times of around 3 minutes, at two different magnifications. This allows the relaxation of the cell & sand system for the first time to be quantified. Advanced image correlation tools are used to quantify the rearrangements of the soil skeleton during loading and the subsequent relaxation. The results indicate that the magnitude of grain displacements during relaxation, associated to ≈4% reduction in externally measured axial stress under oedometric loading, falls below 0:01D50. It can, therefore, be concluded that the relaxation step required prior to an X-ray scan during an in-situ geomechanical experiment on dry sand does not lead to appreciable uncertainties.

        .. figure:: images/publications/dijkstra2019.png
                    :align: center

.. dropdown:: 2019 - Roubin, E. et al., Cement and Concrete Composites

        **Title:** The colours of concrete as seen by X-rays and neutrons.

        **Authors:** Roubin, E., Ando, E., & Roux, S.

        **Journal:** Cement and Concrete Composites, 104, 103336.

        **Link:** `Link <https://www.sciencedirect.com/science/article/pii/S0958946518313489?casa_token=f2ztNwP_yU0AAAAA:g0TzPwkMWozo_2aDdJp4mVgd9VuXwSrIWxojDd_JPxuEyILBR5XR8lUI4D4wjvqliSpDbY4T8Q>`_

        **Tags:** `Multimodal registration <https://spam-project.gitlab.io/spam/tutorials/tutorial-09-multimodal_registration.html>`_,

        **Abstract:**

        Both x-ray tomography and neutron tomography give very detailed insight in the microstructure of concrete. However, their different contrasts, due to different compositional sensitivities, make one modality more relevant for some features. The present study shows that both types of images acquired on the same specimen may be registered onto each other, after the statistical joint distribution of absorption coefficients has been learned. A Gaussian mixture model has been used to identify up to five different phases having different signatures. A staggered algorithm consisting in i) adjusting the joint histogram to fit phases and their variances and ii) registering the two 3D images onto each other, within a multi-scale algorithm is presented in details. The analysed experimental data illustrates the benefit of using jointly both modalities as compared to their parallel usage.

        .. figure:: images/publications/roubin2019.png
                    :align: center

.. dropdown:: 2019 - Wiebicke, M. et al., Granular Matter

        **Title:** A benchmark strategy for the experimental measurement of contact fabric.

        **Authors:** Wiebicke, M., Andò, E., Šmilauer, V., Herle, I., & Viggiani, G.

        **Journal:** Granular Matter, 21(3), 54.

        **Link:** `Link <https://link.springer.com/article/10.1007/s10035-019-0902-x>`_

        **Tags:** `Fabric analysis <https://spam-project.gitlab.io/spam/tutorials/tutorial-08-contacts.html#tutorial-contacts-calculation>`_

        **Abstract:**

        The mechanics of granular materials can be better understood by experimental measurement of fabric and its evolution under load. X-ray tomography is a tool that is increasingly used to acquire three-dimensional images and thus, enables such measurements. Our previous study on the metrology of interparticle contacts revealed that the most common approaches either fail to accurately measure contact fabric or introduce a strong bias. Methods to improve these measurements (i.e., the detection and orientation of contacts) were proposed and validated. This work develops a strategy to benchmark image analysis tools that can be used for the determination of contact fabric from tomographic images. The discrete element method is used to create and load a reference specimen for which the fabric and its evolution is precisely known. Chosen states of this synthetic specimen are turned into realistic images taking into account inherent image properties, such as the partial volume effect, blur and noise. The application of the image analysis tools on these images validates the findings of the metrological study and highlights the importance of addressing the identified shortcomings, i.e., the systematical over-detection of contacts and the strong bias of orientations when using common watersheds.

        .. figure:: images/publications/wiebicke2019.png
                    :align: center

.. dropdown:: 2019 - Wiebicke, M., et al., E3S Web of Conferences

        **Title:** Measuring the fabric evolution of particulate media during load reversals in triaxial tests.

        **Authors:** Wiebicke, M., Andò, E., Herle, I., & Viggiani, G.

        **Journal:** E3S Web of Conferences (Vol. 92, p. 03001). EDP Sciences.

        **Link:** `Link <https://www.e3s-conferences.org/articles/e3sconf/abs/2019/18/e3sconf_isg2019_01001/e3sconf_isg2019_01001.html>`_

        **Tags:** `Fabric analysis <https://spam-project.gitlab.io/spam/tutorials/tutorial-08-contacts.html#tutorial-contacts-calculation>`_

        **Abstract:**

        The behaviour of granular materials upon load reversal is not yet fully understood. In order to experimentally reveal the microstructure of a specimen subjected to load-unload cycles, a triaxial compression test on lentils is carried out in the x-ray scanner. Before analysing the acquired tomographies, a benchmark analysis is conducted to validate the image analysis tools that are used to extract the fabric from these images. The contact fabric evolves strongly with the shearing in the experiment on lentils. However, only very slight changes of the anisotropy within the cycles are observed.

        .. figure:: images/publications/wiebicke2019b.png
                    :align: center

