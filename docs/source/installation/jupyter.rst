.. _spamJupyter:

Install a **spam** jupyter kernel
====================================

You first need to do the user installation :ref:`spamUserInstall`

If you work in a virtualenv, you have to add `ipykernel` in it :

.. code-block:: console

    (spam-venv) $ pip install ipykernel

and declare your virtualenv in Jupyter :

.. code-block:: console

    (spam-venv) $ python -m ipykernel install --user --name=spam

Then you should be able to select this kernel from Jupyter and download the Jupyter Notebook files from the examples.
