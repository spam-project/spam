.. _runSpamWithDocker:

Use the **spam** docker image
==============================

With this method you don't need to install **spam** to run it.
You only need to install `docker`_.

.. _Docker: https://docs.docker.com/get-docker/

We've created a docker image based on `debian 10 bullseye` and `python 3.10` with **spam**
and all its dependencies already installed on it. This way you won't have to
install anything to run your programs.

1. Pull the image
---------------------
You can pull the image and rename it to `spam` for conveniency:

.. code-block:: console

    $ docker pull registry.gitlab.com/spam-project/docker/spam
    $ docker image tag registry.gitlab.com/spam-project/docker/spam spam
    $ docker rmi registry.gitlab.com/spam-project/docker/spam

2. Input/output folder on the host
-------------------------------------
On your computer (hearafter refered to as the `host`), have a folder with all your input data and scripts.
The output files will be written in this folder too, as if you were running your script
directly on the host.

**WARNING**: avoid reading and writing outside of this directory.

For example your folder can look like this:

.. code-block:: console

    /home/me/mydata
        im1.tif
        im2.tif
        myscript.py

In the following docker commands you will see the option

.. code-block:: console

    -v /home/me/mydata:/work

that binds your host folder `/home/me/mydata` with the container. It means that all inputs and outputs will be read and written by the container in `/home/me/mydata`.
Keep in mind to change `/home/me/mydata` with the actual path of your working directory on your computer.

3. Execute your script or a spam command
-----------------------------------------
You can now execute your script with the following command:

.. code-block:: console

    $ docker run --rm -v /home/me/mydata:/work spam python myscript.py

You can also directly use **spam** commands

.. code-block:: console

    $ docker run --rm -v /home/me/mydata:/work spam spam-ldic --help

4. Jupyter notebooks
---------------------
The image embeds a jupyter lab server accessible at `http://127.0.0.1:8888` with the command

.. code-block:: console

    $ docker run --rm -p 8888:8888 -v /home/me/mydata:/work spam
