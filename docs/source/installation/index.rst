.. _installationInstructions:

Installation
=============

**spam** works on **Python 3.8+** (older python versions are no longer maintained).

If you want to "just" use spam you should follow the :ref:`spamUserInstall`.
If you want to setup a development version to contribute to **spam** or use a feature in a development branch, please follow the :ref:`spamDeveloperInstall`

.. toctree::
    :maxdepth: 1

    users
    dev
    jupyter
    docker

