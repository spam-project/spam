.. _howToContribute:

##################
How to Contribute
##################

Everyone is welcome to contribute to this tool.
Write some documentation, or contribute your beautiful examples!
If you add citations they should be in APA format.

Conventions
------------------------

The following programming conventions are used in *spam*:

    * General programming guidelines
        * We use `camelCase` for function and variable names
        * Readability and understandability come first
        * When manipulating files, be careful to use `os.path.join(DIR, FILENAME)` instead of `DIR + FILENAME`
        * C/C++ bindings with *pybind11* for critical pieces of code are accepted, but are not expected to be called directly by users, so should be accompanied by python helper functions which prepare data for execution with C/C++
        * Parallelisation is done with the multiprocessing library in python, and not in C/C++

    * Documentation:
        * Should be clear and refer to scientific literature where possible (and where it helps understanding)
        * *i.e.*, a *docstring* should accompany each function

    * Layout of *spam*
        * Atomic functions are grouped into toolkits in /tools
        * C/C++ code is kept all together in a /src folder in each directory inside /tools and is compiled into a single shared object \*.so called toolToolkit
        * /scripts contains more complex code that calls functions and which are called on the command line with flags or a settings file
          These functions should be self-explanatory and monkey-safe
          /tools/helpers/optionsParser.py contains all the settings for command line arguments
        * /tests contains tests to be run against each function
        * /examples contains python examples showing (off) pieces of functionality that logically go together for users to adapt and change
        * /tools/data contains examples of 3D data for tests

    * Functions:
        * Should have clear names
        * Should be understandable by themselves by moderately experienced users
        * Should take in simple data structures (numpy arrays, lists, numbers, strings), avoid Classes *etc*
        * Should require as few inputs as possible, with intelligent setting of optional variables
        * Should ideally have an example
        * When called don't use the full path of the function, *i.e.*, prefer `spam.DIC.register()` instead of `spam.DIC.registration.register()`

    * Scripts:
        * Should have short and meaningful names beginning with `spam-`

    * Tests:
        * For function we are aiming for 100% coverage, see here, so check all cases:
            https://spam-project.gitlab.io/spam/coverage/
        * Write your output files (with `os.path.join`, see above) into a directory called `.testDump` then remove the while directory during `tearDown()`


Technical workflow
------------------------
Our open access repository on gitlab.com is here: https://gitlab.com/spam-project/spam

You're free to fork it! 
However, as we see *spam* as a community of users and developers, if you'd like to contribute to *spam*, first reach out to us (through our matrix room or by creating an issue).
First, we will add you as a member of the project. 
Then, you can create your feature branch and we will be happy to assist you in the merge request.

.. code-block:: console

    # Create your feature branch
    $ git switch -c my-branch 

    # Or move to an existing branch
    $ git switch my-branch 

    # Install the pre-commit that automatically reformats your file to our standard before each commit
    $ pre-commit install 

.. code-block:: console

    # Add your feature in the code and push
    $ touch myfile
    $ git add myfile
    $ git commit -m "my feature"
    $ git push

    # You can push as many commits as you wish
    # Over time, it's good to keep your branch regularly up to date with master
    $ git fetch
    $ git merge origin/master # merge master into my-branch (this method will create an extra commit)
    # At this point you might have some conflicts to resolve
    # The more often you update your branch, the less likely to happen
    $ git push # push the merge commit 


Any push to your branch will propose you a merge request, which will allow us to merge your branch to master.
Before doing this, check that your feature is covered in the tests (we can help with this part) and that they all pass.

.. code-block:: console

    $ pytest # for all tests
    $ pytest tests/test_myfunction.py # for a specific test 

You can also check the code coverage by: 

.. code-block:: console

    $ coverage run -m pytest
    $ coverage html
	$ coverage report

If you modify the documentation, please check that it's still building and renders correctly by building it locally:

.. code-block:: console
    
    $ sphinx-apidoc -o docs/source src/spam
    $ make -C docs/source html #

You can check the documentation on your browser by opening this file: ./docs/source/_build/html/index.html
