# Continous integration explained

## Docker images

Dockerfiles used for building/testing/delivering are available in [docker-ttk](https://gricad-gitlab.univ-grenoble-alpes.fr/ttk/docker-ttk) repository :

Images are available in the Gricad Docker registry :

* gricad-registry.univ-grenoble-alpes.fr/ttk/docker-ttk
* gricad-registry.univ-grenoble-alpes.fr/ttk/docker-ttk/manylinux 


### Test/build/doc
We use a debian stretch image to build/test/docs (all CI stages except deploy) :
https://gricad-gitlab.univ-grenoble-alpes.fr/ttk/docker-ttk/blob/master/Dockerfile

> For now, integration tests run on python 2.7 (default python on debian stretch). This should change soon<sup>TM</sup>.


### Pypi packages
We use a modified Centos6 base image provided by Pypa[^manylinux] for wheel building/uploading :
https://gricad-gitlab.univ-grenoble-alpes.fr/ttk/docker-ttk/blob/master/Dockerfile-manylinux

> Our wheels are compiled against a old glibc (2.12, released in 2010) to ensure compatibilty. For older glibc, compilation from source is required.


[^manylinux]: see https://github.com/pypa/manylinux

## Pypi upload

We build and upload wheels for the following python version :

*  cp27-cp27m
*  cp27-cp27mu
*  cp35-cp35m
*  cp36-cp36m
*  cp37-cp37m

The script that build the wheels is [build-wheels.sh](https://gricad-gitlab.univ-grenoble-alpes.fr/ttk/spam/blob/master/build-wheels.sh)
