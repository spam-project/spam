#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace py=pybind11;

// int countTetrahedraCGAL(int             numTetrahedrals_numVertices,
//                         int             numTetrahedrals_three,
//                         float*          numTetrahedrals_vertices,
//                         int             numTetrahedrals_numWeights,
//                         float*          numTetrahedrals_weights);

int countTetrahedraCGAL( py::array_t<float> numTetrahedrals_verticesNumpy,
                         py::array_t<float> numTetrahedrals_weightsNumpy,
                         py::array_t<float> numTetrahedrals_alphaNumpy);

// void triangulateCGAL(   int             connectivityMatrix_numVertices,
//                         int             connectivityMatrix_three,
//                         float*          connectivityMatrix_vertices,
//                         int             connectivityMatrix_numWeights,
//                         float*          connectivityMatrix_weights,
//                         int             connectivityMatrix_numTet,
//                         int             connectivityMatrix_four,
//                         unsigned int*   connectivityMatrix_connectivity)

void triangulateCGAL(   py::array_t<float> connectivityMatrix_verticesNumpy,
                        py::array_t<float> connectivityMatrix_weightsNumpy,
                        py::array_t<unsigned int> connectivityMatrix_connectivityNumpy,
                        py::array_t<float> numTetrahedrals_alphaNumpy);
