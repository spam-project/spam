#ifndef __PROJMORPHO_HPP
#define __PROJMORPHO_HPP

#include <vector>   // this include is needed for swig compilation
#include <string.h> // this include is needed for swig compilation

class projmorpho
{

public:
  projmorpho(const std::vector<double> thresholds, std::string name, double volume_ratio_cutoff);

  ~projmorpho(){};

  // These two functions set the field values for each nodes and midpoints of the mesh

  // image
  void set_image(std::vector<std::vector<double>> v_image, std::vector<double> d_image, std::vector<unsigned> n_image, std::vector<double> o_image);
  void compute_field_from_images();
  void compute_field_from_images_loop(const std::vector<std::vector<double>> c_mesh, std::vector<std::vector<double>> &v_mesh);
  std::vector<std::vector<double>> get_image_coordinates();
  std::vector<std::vector<double>> get_image_values();

  // objects
  void set_objects(std::vector<std::vector<double>> objects);
  void compute_field_from_objects();
  void compute_field_from_objects_loop(const std::vector<std::vector<double>> c_mesh, std::vector<std::vector<double>> &v_mesh);
  double _compute_distance_from_object(std::vector<double> object, std::vector<double> point, unsigned int phase);
  std::vector<double> _compute_normal_from_object(std::vector<double> object, std::vector<double> point);

  // mesh
  void set_mesh(std::vector<std::vector<double>> c_mesh, std::vector<unsigned> a_mesh);
  void set_field(std::vector<std::vector<double>> v_mesh);
  std::vector<std::vector<double>> get_mesh_coordinates();
  std::vector<std::vector<double>> get_mesh_values();
  std::vector<unsigned> get_mesh_connectivity();
  std::vector<std::vector<double>> get_interface_coordinates();
  std::vector<std::vector<int>> get_interface_tri_connectivity();
  std::vector<std::vector<int>> get_interface_qua_connectivity();

  // projection
  void projection(bool analytical_orientation);
  std::vector<std::vector<double>> get_materials();

  // files
  void write_feap();
  void write_vtk();
  void write_interfaces_vtk();

private:
  // mesh variables
  unsigned int _n_nodes;                              // defined in get_c_mesh
  unsigned int _n_elem;                               // defined in get_a_mesh
  std::vector<std::vector<double>> _c_mesh;           // mesh coordinates ( n_nodes x 3 )
  std::vector<std::vector<double>> _v_mesh;           // fields values interpolated onto the mesh nodes ( number of phase x n_nodes )
  std::vector<unsigned> _a_mesh;                      // mesh connectivity ("a" stands for adjency)
  std::vector<std::vector<double>> _c_mesh_midpoints; // mesh midpoints coordinates ( (6xn_elmt) x 3 )
  std::vector<std::vector<double>> _v_mesh_midpoints; // fields values interpolated onto the mesh nodes ( number of phase x (6xn_elmt) )

  // interface variables
  unsigned int _int_n_nodes;                     // total number of nodes
  unsigned int _int_n_tri;                       // total number of tirangles
  unsigned int _int_n_qua;                       // toal number of quads
  std::vector<std::vector<double>> _int_nodes;   // coordinates of the nodes (3 x _int_n_nodes) [we have to push back... so shut up]
  std::vector<double> _int_sub_volume_ratio_tri; // sub volume / tet volume (_int_n_nodes)
  std::vector<double> _int_sub_volume_ratio_qua; // sub volume / tet volume (_int_n_nodes)
  std::vector<int> _int_ignored_tri;             // ignored interfaces (0: not ignored 1: sub volumes 2: impossible to compute normal)
  std::vector<int> _int_ignored_qua;             // ignored interfaces (0: not ignored 1: sub volumes 2: impossible to compute normal)
  std::vector<std::vector<int>> _int_a_tri;      // connectivity matrix of the triangles (3 x _int_n_tri)
  std::vector<std::vector<int>> _int_a_qua;      // connectivity matrix of the quad (4 x _int_n_qua)
  std::vector<std::vector<double>> _int_v_tri;   // connectivity matrix of the triangles (_int_n_tri x number of things we want in it)
  std::vector<std::vector<double>> _int_v_qua;   // connectivity matrix of the quad (_int_n_qua x number of things we want in it)

  // field variables
  std::vector<std::vector<double>> _c_image; // field coordinates ( n_nodes x 3 )
  std::vector<std::vector<double>> _v_image; // fields values ( number of phase x n_nodes )
  std::vector<unsigned> _n_image;            // number of nodes in the three directions
  std::vector<double> _d_image;              // total length of the field in the three directions
  std::vector<double> _o_image;              // origin of the field in the three directions

  // objects
  // sphere:   5 [r, ox, oy, oz, p]
  // cylinder: 8 [r, ox, oy, oz, nx, ny, nz, p]
  std::vector<std::vector<double>> _objects; // list of all the objects (number of objects x number of parameter)
  std::vector<unsigned int> _phases_values;

  // global material arrays
  // filling these arrays is more or less the goal of this program
  std::vector<int> _tetra_mat;
  std::vector<double> _tetra_sub_volume;
  std::vector<std::vector<double>> _tetra_orientation;

  // init
  std::string _file_name;          // file name
  double _volume_ratio_cutoff;     // htreshold to cut of interface elements with small sub volumes
  std::vector<double> _thresholds; // thresholds

  void print_error(std::string msg, bool ex);
};

#endif //__PROJMORPH
