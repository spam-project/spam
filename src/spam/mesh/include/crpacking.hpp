#ifndef __CRPACKING_HPP
#define __CRPACKING_HPP

#include <vector> // this include is needed for swig compilation

class crpacking
{

public:
  crpacking(const std::vector<double> parameters,
            const std::vector<double> lengths,
            const std::vector<double> origin,
            const unsigned int inside,
            const std::string file_name,
            const std::string domain);
  ~crpacking(){ std::cout << "<crpacking::~crpacking>" << std::endl; };

  // create objects
  void create_spheres();
  void create_ellipsoids();

  // pack objects
  std::vector<std::vector<double>> pack_spheres(bool vtk);
  void pack_ellipsoids();

  // read objects
  void set_objects(std::vector<std::vector<double>> objects);

  // get
  std::vector<std::vector<double>> get_objects();

  // write vtk
  void write_spheres_vtk(std::string file_name);

private:
  // for packing
  void _set_intersections(unsigned int type); // type == 0 for spheres and type == 1 for ellispoids

  // for packing ellipsoids
  std::vector<double> _determine_sphero(const std::vector<double> &s1, const std::vector<double> &s2);
  std::vector<double> _inter_sphero(const std::vector<double> &s1, const std::vector<double> &s2);

  // geometrical parameters
  std::vector<double> _lengths;    // x, y, z dimensions
  std::vector<double> _origin;     // x, y, z origin
  std::vector<double> _parameters; // parameters of the objects
  bool _inside;                    // packing inside or authorize cuting
  unsigned int _n_phases;          // number of phases (not counting background)
  std::string _domain_type;  // type of the domain where the objects are packed
  std::vector<unsigned int> _phases_values; // aray countaing the phases

  // intersection variables
  unsigned int _int_n;
  std::vector<int> _int_s;
  std::vector<std::vector<double>> _int_p;

  // objects
  std::vector<std::vector<double>> _objects; // list of all the objects
  unsigned int _n_objects;                   // number of objects == _objects.size()

  // file
  std::string _file_name;

  // misc
  void print_error(std::string msg, bool ex);

};

#endif //__CRPACKING
