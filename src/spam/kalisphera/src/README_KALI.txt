#####################
PLEASE READ THIS FILE
#####################
if you don't have time to read all of the file,
please refer to the end of file :
"modifications"

this file goes with the 1.0 release version of kalisphera_c and kaliblur_c

#####################
FILE MAP
#####################
=> kalisphera ?
   -> use
   -> limits
=> kaliblur ?
   -> use
   -> limits
=> requirements
   -> python
   -> compilers
=> modifications
   -> permission
   -> good practices
=> Authors
   -> Python version and Maths
   -> C version
   -> how to contact us
   -> sources location
   
#####################
KALISPHERA
#####################
kalisphera is a tool, originaly writen in Python (for more, see "Authors"),
about to create sphere in a 3D voxel (volume element, 3D pixels) array.
next part is only about kalisphera_c, but some of the tips can be applyed to 
kalisphera Python.

# Use #
the C version of kalisphera creates a sphere by splitting it in cubes.
each voxel represents a cube, and the value of the voxel is the report between
volume of the cube (1.0) and the volume of the part of the sphere contained in the cube.

From both python and C call, you have to create the array (volume) empty first 
(filled with "0" value"), and then give it to kalisphera_c.
The allocation and desallocation (in C only) is your matter, kalisphera only changes values of the array.

the return value is the difference between sum of all voxel changed and total volume expected.

# limits #
Some error can occur in voxel values computation. these error can be significant (>1/1000 of total volume), 
and can be detected using the return value of the kalisphera function.
the voxelValByForce function can be also used to determinate an estimated upper and lower values by bruteforce.
Be careful while using bruteforce, this can slow a lot the speed of the program.

The other limit of kalisphera is about sphere collision : you cannot add two sphere in the same array if the spheres are in contact :
you will end with false values because the second sphere added will override parts of the first sphere.
If you want to do it, you have to use two arrays that you merge with your own methods.

#####################
KALIBLUR
#####################
kaliblur is a tool used to apply a gaussian blur on a 3D picture (volume)
it only has a C version, Python version is in-built in kalispera Python.

# use #
Kaliblur uses a simple gaussian blur,
you can call it using sigma value or pre-built blur matrix.
note that you can add bounds to the blur when calling it with sigma value.

# limits #
When there are values > 0 in the bounds range (see below for what is bounds range), there can be a gain or loss
in the total value of the volume.

bounds range are all the voxels that are included in the range [boundBegin - blurRadius ; boundEnd + blurRadius] and
not included int the range [boundBegin + blurRadius ; boundEnd - blurRadius]

#####################
REQUIREMENTS
#####################

# Python #
to use the given bindings between kalisphera_c or kaliblur_c and Python, you will need 
-Python
-Numpy
-Cython
Obviously you can also recreate you own bindings, with others languages or simply in a 
different way that our are done.

# compilers #
When downloading the file for the first time, you will need to build C source into a program.
If using given Python bindings, just run the "setup" and/or "kaliblur_setup" files with
the command line given in comment in each of these files.

If you want to use kalisphera_c or kaliblur_c in another way, (maybe directly in C),
you can use any C compiler that will accept these sources (they are a lot !)

#####################
MODIFICATIONS
#####################

# permissions #
As the code is under GPL  licence, you can modify and redistribute it as you want,
the only condition is that the code you produce from the kali code is also under
GPL licence.

you can also mention 

# good practices #
following tips are not obligation, but respecting them can help us improve our code

If you do significant modifications, as speed increase, corrections, or functionnalities adding,
it would be good to let us know, by sending an e-mail to any of the authors of kalisphera (see below : authors).

Please also mention the name and email of one of the original authors (see below : authors),
and the following section : "good practices" in a file of your code.

#####################
AUTHORS
#####################

# Python version and Math #
Edward Ando' :  edward.ando@3sr-grenoble.fr
Alessandro Tengattini : alessandro.tengattini@3sr-grenoble.fr

# C version #
Felix Bertoni : felix.bertoni987@gmail.com

# how to contact us #
you can contact us for any question or any request
If the request is about adding new features to the code, prefer ask Felix.
If the question is about kalisphera, use and/or mathematics, prefere ask Alessandro or Edward.

Please mention "kalisphera" or "kaliblur" as first word in the object of your mail,
followed by a very short summary (few words) of the mail content.

# sources location #
you can find the original sources of kalisphera at :
https://forge.3sr-grenoble.fr/repos/kalisphera/

Please be careful, this URL can be changed over time.

#####################
THANKS FOR READING
#####################