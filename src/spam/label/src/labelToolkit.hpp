#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace py=pybind11;

namespace labels
{
    /* If you change this, remember to change the typedef in tools/labelToolkit.py */
    typedef unsigned int     label;
    typedef unsigned int     contact;
//     typedef unsigned short     label;
}



void boundingBoxes( py::array_t<labels::label> volLab, py::array_t<unsigned short> boundingBoxes );

void centresOfMass( py::array_t<labels::label> volLab,
                    py::array_t<unsigned short> boundingBoxes,
                    py::array_t<float> centresOfMass,
                    int minVolFiltVX );

void volumes(    py::array_t<labels::label> volLabNumpy,
                 py::array_t<unsigned short> boundingBoxesNumpy,
                 py::array_t<unsigned int> volumesNumpy
             );

void momentOfInertia(  py::array_t<labels::label> volLabNumpy,
                       py::array_t<unsigned short> boundingBoxesNumpy,
                       py::array_t<float> centresOfMassNumpy,
                       py::array_t<float> momentOfInertiaEigenValuesNumpy,
                       py::array_t<float> momentOfInertiaEigenVectorsNumpy
                    );

void labelToFloat(   py::array_t<labels::label> volLabNumpy,
                     py::array_t<float> labelFloatsNumpy,
                     py::array_t<float> volOutputNumpy
                   );


void relabel(         py::array_t<labels::label> volLabNumpy,
                      py::array_t<labels::label> labelMapNumpy
                    );



void tetPixelLabel(  py::array_t<labels::label> volNumpy,    // image
                     py::array_t<labels::label> connectivityNumpy,  // Connectivity Matrix -- should be nTetrahedra * 4
                     py::array_t<float> nodesNumpy,   // Tetrahedra Points --   should be nNodes      * 3
                     int numThreads
                   );

void setVoronoi(    py::array_t<labels::label> volLabNumpy,
                    py::array_t<float> poreEDTNumpy,
                    py::array_t<labels::label> volLabOutNumpy,
                    py::array_t<int> indicesSortedNumpy,
                    py::array_t<int> indicesNumpy
                    //                     float distThresh
                    );

void labelContacts(    py::array_t<labels::label> volLabNumpy,
                       py::array_t<labels::contact> volContactsNumpy,
                       py::array_t<unsigned char> ZNumpy,
                       py::array_t<labels::contact> contactTableNumpy,
                       py::array_t<labels::label> contactingLabelsNumpy
                    );
