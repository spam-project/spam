#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace py=pybind11;

void average(   py::array_t<float> imIn,
                py::array_t<float> imOu,
                py::array_t<float> stEl );

void variance(  py::array_t<float> imIn,
                py::array_t<float> imOu,
                py::array_t<float> stEl );

void hessian(   py::array_t<float> hzz,
                py::array_t<float> hzy,
                py::array_t<float> hzx,
                py::array_t<float> hyz,
                py::array_t<float> hyy,
                py::array_t<float> hyx,
                py::array_t<float> hxz,
                py::array_t<float> hxy,
                py::array_t<float> hxx,
                py::array_t<float> valA,
                py::array_t<float> valB,
                py::array_t<float> valC,
                py::array_t<float> valAz,
                py::array_t<float> valAy,
                py::array_t<float> valAx,
                py::array_t<float> valBz,
                py::array_t<float> valBy,
                py::array_t<float> valBx,
                py::array_t<float> valCz,
                py::array_t<float> valCy,
                py::array_t<float> valCx );
