# -*- coding: utf-8 -*-

import unittest
import os
import tifffile
import numpy

import spam.label
import spam.kalisphera

VERBOSE = True

class TestAll(spam.helpers.TestSpam):

    def test_ITKwatershed(self):
        # First try without markers
        im = numpy.zeros([50, 50, 50], dtype='<f8')

        # Generate and binarise two touching balls on the z-plane
        centres = [[25, 25, 15], [25, 25, 35]]
        radii = [10, 10]
        spam.kalisphera.makeSphere(im, centres, radii)
        # Threshold at 0.5
        im[im > 0.5]  = 1.0
        im[im <= 0.5] = 0.0
        # Watershed
        lab = spam.label.watershed(im)
        self.assertEqual(lab.max(), 2)
        equivRadii = spam.label.equivalentRadii(lab)
        self.assertAlmostEqual(equivRadii[1], radii[0], places=1)
        self.assertAlmostEqual(equivRadii[2], radii[1], places=1)

        # Let's compute markers
        m1 = numpy.zeros([50, 50, 50], dtype='<f8')
        m2 = numpy.zeros([50, 50, 50], dtype='<f8')

        # Generate and binarise two small internal balls
        spam.kalisphera.makeSphere(m1, [25, 25, 15], 3)
        spam.kalisphera.makeSphere(m2, [25, 25, 35], 3)
        # Threshold at 0.5
        m1[m1 > 0.5]  = 1.0
        m1[m1 <= 0.5] = 0.0
        m2[m2 > 0.5]  = 2.0
        m2[m2 <= 0.5] = 0.0
        markers = m1 + m2

        # Watershed with markers
        lab = spam.label.watershed(im, markers=markers)
        self.assertEqual(lab.max(), 2)
        equivRadii = spam.label.equivalentRadii(lab)
        self.assertAlmostEqual(equivRadii[1], radii[0], places=1)
        self.assertAlmostEqual(equivRadii[2], radii[1], places=1)

if __name__ == '__main__':
    unittest.main()
