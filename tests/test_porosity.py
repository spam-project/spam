# -*- coding: utf-8 -*-

import unittest
import numpy

import spam.measurements
import spam.datasets

class TestAll(spam.helpers.TestSpam):

    # Testing the reproducability of the mean porosity in greylevel image , image = snow from datasets
    def test_porosity(self):
        im = spam.datasets.loadSnow()
        snowPoreValue = 11648
        snowSolidValue = 32895
        ### compute overall porosity
        meanPorosity = numpy.true_divide((100*(im.mean()-snowSolidValue)),(snowPoreValue-snowSolidValue))
        porosity = spam.measurements.porosityField(im, poreValue=snowPoreValue, solidValue=snowSolidValue, nodeSpacing=25, hws=12, showGraph="True", maskValue=65535)
        print(meanPorosity)

        print(porosity.mean())
        self.assertAlmostEqual(meanPorosity, porosity.mean(), places=2)

        # For completeness run with two HWSs
        porosity = spam.measurements.porosityField(im, poreValue=snowPoreValue, solidValue=snowSolidValue, hws=[12, 10])
        self.assertEqual(len(porosity.shape), 4)

    # Test when poreValue>solidValue, should give an error print
    def test_porosity1(self):
        im =spam.datasets.loadSnow()
        snowPoreValue = 32895
        snowSolidValue = 11648
        meanPorosity = numpy.true_divide((100*(im.mean()-snowPoreValue)),(snowSolidValue-snowPoreValue))
        porosity = spam.measurements.porosityField(im, poreValue=snowPoreValue, solidValue=snowSolidValue, nodeSpacing=25, hws=12)
        print(meanPorosity)
        print(porosity.mean())
        self.assertAlmostEqual(meanPorosity, porosity.mean(), places=2)

    # Test when poreValue=solidValue, should give an error print
    def test_porosity2(self):
        im = spam.datasets.loadSnow()
        snowPoreValue = 32895
        snowSolidValue = snowPoreValue
        porosity = spam.measurements.porosityField(im, poreValue=snowPoreValue, solidValue=snowSolidValue, nodeSpacing=25, hws=12)
        self.assertEqual(porosity,None)

    # Test when poreValue=None, should give an error print
    def test_porosity3(self):
        im = spam.datasets.loadSnow()
        porosity = spam.measurements.porosityField(im, solidValue=30000, nodeSpacing=25, hws=12)
        self.assertEqual(porosity,None)

    # Test when solidValue=None, should give an error print
    def test_porosity4(self):
        im = spam.datasets.loadSnow()
        porosity = spam.measurements.porosityField(im, poreValue=30000, nodeSpacing=25, hws=12)
        self.assertEqual(porosity,None)

if __name__ == '__main__':
        unittest.main()
