import unittest

import numpy
import scipy.ndimage
import spam.datasets
import spam.DIC
import spam.helpers


class TestAll(spam.helpers.TestSpam):
    def test_binning(self):
        snowB1 = spam.datasets.loadSnow()
        snowB2spam = spam.DIC.binning(snowB1, 2)
        snowB2zoom = scipy.ndimage.zoom(snowB1, 0.5, order=1)
        # Make sure the mean value is OK
        self.assertAlmostEqual(snowB1.mean() / snowB2spam.mean(), 1.0, places=2)

        # Make sure the difference between b2 spam and b2 scipy is not too different
        self.assertAlmostEqual(
            numpy.sum(numpy.square(snowB2spam - snowB2zoom)) / ((snowB2spam.mean() ** 2) * len(snowB2zoom.ravel())),
            0.0,
            places=1,
        )

        # Make sure the size is half
        self.assertEqual(
            (numpy.ones(3, dtype="<f4") * 2).tolist(),
            (numpy.array(snowB1.shape) / numpy.array(snowB2spam.shape)).tolist(),
        )

        snowB2spamF4 = spam.DIC.binning(snowB1.astype("f4"), 2)
        self.assertAlmostEqual(snowB2spam.mean() / snowB2spamF4.mean(), 1.0, places=2)
        # cast it to f8 and make sure it's cast back to float:
        snowB2spamF8 = spam.DIC.binning(snowB1.astype("f8"), 2)
        self.assertEqual(numpy.sum(snowB2spamF4 - snowB2spamF8), 0)

        # Try 8b version, same tests as for first one...
        snow8bB1 = spam.helpers.imageManipulation.rescaleToInteger(snowB1.astype("f4"), nBytes=1, scale=[0, 65535])
        snow8bB2spam = spam.DIC.binning(snow8bB1, 2)
        snow8bB2zoom = scipy.ndimage.zoom(snow8bB1, 0.5, order=1)

        # Make sure the mean value is OK
        self.assertAlmostEqual(snow8bB1.mean() / snow8bB2spam.mean(), 1.0, places=2)

        # Make sure the difference between b2 spam and b2 scipy is not too different
        self.assertAlmostEqual(
            numpy.sum(numpy.square(snow8bB2spam - snow8bB2zoom)) / ((snow8bB2spam.mean() ** 2) * len(snow8bB2zoom.ravel())),
            0.0,
            places=1,
        )

        # Make sure the size is half
        self.assertEqual(
            (numpy.ones(3, dtype="<f4") * 2).tolist(),
            (numpy.array(snow8bB1.shape) / numpy.array(snow8bB2spam.shape)).tolist(),
        )

        # Ask for a crop and centre, and compare to hand-made solution
        snowB2spam, crop, centre = spam.DIC.binning(snowB1, 2, returnCropAndCentre=True)
        self.assertEqual(
            centre.tolist(),
            numpy.floor((numpy.array(snowB1.shape) - 1) / 2.0).tolist(),
        )
        self.assertEqual(numpy.zeros(3).tolist(), crop[0].tolist())
        self.assertEqual(numpy.zeros(3).tolist(), crop[1].tolist())

        # Try a 2D slice
        oneSliceB1 = snowB1[snow8bB1.shape[0] // 2]
        oneSliceB2 = spam.DIC.binning(oneSliceB1, 2)
        self.assertAlmostEqual(oneSliceB1.mean() / oneSliceB2.mean(), 1.0, places=2)
        self.assertEqual(
            (numpy.ones(2, dtype="<f4") * 2).tolist(),
            (numpy.array(oneSliceB1.shape) / numpy.array(oneSliceB2.shape)).tolist(),
        )


if __name__ == "__main__":
    unittest.main()
