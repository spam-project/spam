import unittest

import numpy
import spam.helpers
import spam.mesh


class TestAll(spam.helpers.TestSpam):
    """Test the projection tools (projmorpho)
    The two main situations tested are:
    1. test canonical functions of projmorpho on single elements
    2. an analytical construction of the distance field (crpacking)
    3. a construction of the distance field based on binary images (spam distField)
    4. project multiple spheres with multiple phases
    """

    def test_crpacking(self):
        """Directly test cpp function of crpacking"""

        # parameters
        #  [rmin, rmax, volume fraction of the phase, phase number]
        phase_a = [0.01, 0.02, 0.4, 1]
        phase_b = [0.02, 0.05, 0.6, 2]
        # [total volume fraction, rejection length]
        param = [0.05, 0.005] + phase_a + phase_b
        # type, size and origin of the domain
        domain = "cube"
        lengths = [1.0, 1.0, 1.0]
        origin = [0.0, 0.0, 0.0]
        # force spherses inside domain
        inside = 1
        # file name
        fileName = "crpacking"

        # TEST 1: pack two sets of spheres
        cr = spam.mesh.meshToolkit.crpacking(param, lengths, origin, inside, fileName, domain)

        cr.createSpheres()
        cr.packSpheres()
        cr.writeSpheresVTK()
        objs = cr.getObjects()

        # check radius
        rmin = [phase_a[0], phase_b[0]]
        rmax = [phase_a[1], phase_b[1]]
        for obj in objs:
            p = int(obj[-1]) - 1
            self.assertTrue(obj[0] >= rmin[p])
            self.assertTrue(obj[0] <= rmax[p])

        # TEST 2: set objects from previous packing
        cr = spam.mesh.meshToolkit.crpacking(param, lengths, origin, inside, fileName + "bis", domain)
        cr.setObjects(objs)
        cr.packSpheres()
        objs = cr.getObjects()

        # check radius
        rmin = [phase_a[0], phase_b[0]]
        rmax = [phase_a[1], phase_b[1]]
        for obj in objs:
            p = int(obj[-1]) - 1
            self.assertTrue(obj[0] >= rmin[p])
            self.assertTrue(obj[0] <= rmax[p])

    def test_packingSpheres(self):

        # test one phase in a cube
        volumeFraction = 0.1
        rejectionLength = 1.0
        phases = [[5.0, 8.0, 1.0]]
        lengths = [100, 120, 140]
        origin = [0, 10, -5]
        spheres = spam.mesh.packSpheres(volumeFraction, rejectionLength, phases, origin=origin, lengths=lengths, vtk="packing-1")
        # check that all spheres are in the cube
        # and that radius is in the bounds
        rmin = [p[0] for p in phases]
        rmax = [p[1] for p in phases]
        for sphere in spheres:
            center = sphere[1:4]
            radius = sphere[0]
            phase = int(sphere[4]) - 1
            self.assertTrue(radius >= rmin[phase])
            self.assertTrue(radius <= rmax[phase])
            self.assertTrue(center[0] >= origin[0])
            self.assertTrue(center[0] < lengths[0] + origin[0])
            self.assertTrue(center[1] >= origin[1])
            self.assertTrue(center[1] < lengths[1] + origin[1])
            self.assertTrue(center[2] >= origin[2])
            self.assertTrue(center[2] < lengths[2] + origin[2])

        # test two phases in a cube
        volumeFraction = 0.1
        rejectionLength = 1.0
        phases = [[5.0, 8.0, 0.5, 1.0], [8.0, 8.0, 0.5, 3.0]]
        lengths = [100, 120, 140]
        origin = [0, 10, -5]
        spheres = spam.mesh.packSpheres(volumeFraction, rejectionLength, phases, origin=origin, lengths=lengths, vtk="packing-1")
        # check that all spheres are in the cube
        # and that radius is in the bounds
        rmin = [p[0] for p in phases]
        rmax = [p[1] for p in phases]
        phaseValues = {int(p[-1]): i for i, p in enumerate(phases)}
        for sphere in spheres:
            center = sphere[1:4]
            radius = sphere[0]
            p = phaseValues[int(sphere[4])]
            self.assertTrue(radius >= rmin[p])
            self.assertTrue(radius <= rmax[p])
            self.assertTrue(center[0] >= origin[0])
            self.assertTrue(center[0] < lengths[0] + origin[0])
            self.assertTrue(center[1] >= origin[1])
            self.assertTrue(center[1] < lengths[1] + origin[1])
            self.assertTrue(center[2] >= origin[2])
            self.assertTrue(center[2] < lengths[2] + origin[2])

        # test two granulo with one phase
        volumeFraction = 0.1
        rejectionLength = 1.0
        phases = [[5.0, 7.0, 0.5, 1], [7.0, 8.0, 0.5, 1]]
        lengths = [100, 120, 140]
        origin = [0, 10, -5]
        spheres = spam.mesh.packSpheres(volumeFraction, rejectionLength, phases, origin=origin, lengths=lengths, vtk="packing-1")
        # check that all spheres are in the cube
        # and that radius is in the bounds
        rmin = min([p[0] for p in phases])
        rmax = max([p[1] for p in phases])
        for sphere in spheres:
            center = sphere[1:4]
            radius = sphere[0]
            self.assertTrue(radius >= rmin)
            self.assertTrue(radius <= rmax)
            self.assertTrue(center[0] >= origin[0])
            self.assertTrue(center[0] < lengths[0] + origin[0])
            self.assertTrue(center[1] >= origin[1])
            self.assertTrue(center[1] < lengths[1] + origin[1])
            self.assertTrue(center[2] >= origin[2])
            self.assertTrue(center[2] < lengths[2] + origin[2])

        # raise error with wrong number of parameters
        with self.assertRaises(Exception):
            spam.mesh.packSpheres(0.01, 1.0, [[5.0, 8.0, 0.5, 1, "yolo"]])

        # test one phase in a cylinder
        volumeFraction = 0.1
        rejectionLength = 1.0
        phases = [[5.0, 8.0, 1.0]]
        lengths = [200, 0, 400]
        origin = [0, 50, -5]
        spheres = spam.mesh.packSpheres(volumeFraction, rejectionLength, phases, domain="cylinder", origin=origin, lengths=lengths, vtk="packing-1")

        # check that all spheres are in the cylinder
        # and that radius is in the bounds
        rmin = phases[0][0]
        rmax = phases[0][1]
        cylinderO = [origin[i] + 0.5 * lengths[0] for i in [0, 1]]
        zmin = origin[2]
        zmax = origin[2] + lengths[2]
        cylinderR = lengths[0]
        for sphere in spheres:
            radius = sphere[0]
            self.assertTrue(radius >= rmin)
            self.assertTrue(radius <= rmax)
            self.assertTrue(sphere[4] >= zmin)
            self.assertTrue(sphere[4] < zmax)
            distance = numpy.linalg.norm(numpy.array(cylinderO) - sphere[1:3])
            self.assertTrue(distance <= cylinderR)

    def test_doc(self):

        # doc spam.mesh.packSphere()
        volumeFraction = 0.1
        rejectionLength = 1.0
        # phase 1: rmin = 5, rmax = 6.5, volume fraction = 0.6 (of total volume fraction)
        # phase 2: rmin = 6.5, rmax = 8, volume fraction = 0.4 (of total volume fraction)
        phases = [[5.0, 6.5, 0.6], [6.5, 8.0, 0.4]]
        # cylinder going from -5 to 135 in z
        # with a base radius 50 and center [50, 60
        domain = "cylinder"
        lengths = [100, 0, 140]
        origin = [0, 10, -5]
        # generate and pack spheres
        spam.mesh.packSpheres(volumeFraction, rejectionLength, phases, domain=domain, origin=origin, lengths=lengths, vtk="packing-1")

    def test_packingSpheresFromList(self):

        # STEP 1: create spheres with crpacking
        volumeFraction = 0.2
        rejectionLength = 1.0
        # phase 1: rmin = 5, rmax = 6.5, volume fraction = 0.6 (of total volume fraction)
        # phase 2: rmin = 6.5, rmax = 8, volume fraction = 0.4 (of total volume fraction)
        phases = [[5.0, 6.5, 0.6], [6.5, 8.0, 0.4]]
        # cylinder going from -5 to 135 in z
        # with a base radius 50 and center [50, 60
        domain = "cube"
        lengths = [100, 100, 100]
        origin = [0, 0, 0]
        # generate and pack spheres
        spheres = spam.mesh.packSpheres(volumeFraction, rejectionLength, phases, domain=domain, origin=origin, lengths=lengths, vtk="packing-1")

        # STEP 2: use packObjectsFromList
        # spheres = spam.mesh.packObjectsFromList(spheres, rejectionLength, vtk="packing-1")
        domain = "cylinder"
        lengths = [70, 0, 150]
        origin = [0, 0, -25]
        spheres = spam.mesh.packObjectsFromList(spheres, rejectionLength, domain=domain, origin=origin, lengths=lengths, vtk="packing-2")


if __name__ == "__main__":
    # spam.helpers.TestSpam.DEBUG = True
    unittest.main()
