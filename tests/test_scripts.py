import os
import subprocess
import unittest

import h5py
import numpy
import spam.datasets
import spam.deformation
import spam.DIC
import spam.helpers
import spam.kalisphera
import spam.label
import spam.mesh
import tifffile

# This for hiding script output
FNULL = open(os.devnull, "w")

# This for showing it
# FNULL = None

# We also check 2D slice so don't displace in Z or rotate around an axis other than Z
refTranslation = numpy.array([0.0, 15, 0.0])
refRotation = numpy.array([3.0, 0.0, 0.0])


class TestAll(spam.helpers.TestSpam):
    def test_deformImage(self):
        im = spam.datasets.loadSnow()[:40, :40, :40]
        tifffile.imwrite("snow-ref.tif", im)
        mask = numpy.ones(im.shape, dtype="bool")
        tifffile.imwrite("snow-mask.tif", mask)

        Phi = spam.deformation.computePhi({"r": [5.0, 0.0, 0.0], "z": [1.05, 1.0, 1.0]})

        imDef = spam.DIC.applyPhi(im, Phi=Phi)

        tifffile.imwrite("snow-def-orig.tif", imDef)

        exitCode = subprocess.call(
            ["spam-reg", "-m", "8", "-od", ".", "snow-ref.tif", "snow-def-orig.tif"],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(
            [
                "spam-passPhiField",
                "-pf",
                "snow-ref-snow-def-orig-registration.tsv",
                "-im1",
                "snow-ref.tif",
                "-ns",
                "4",
                "-od",
                ".",
            ]
        )
        self.assertEqual(exitCode, 0)

        # Run the script with default pixel mode and mask
        exitCode = subprocess.call(
            [
                "spam-deformImage",
                "-pf",
                "snow-ref-snow-def-orig-registration-passed-ns4.tsv",
                "snow-ref.tif",
                "-rst",
                "1",
                "-nn",
                "4",
                "-mf2",
                "snow-mask.tif",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        imDefPix = tifffile.imread("snow-ref-def.tif")
        self.assertTrue(numpy.abs((imDefPix - imDef)[8:-8, 8:-8, 8:-8]).mean() < 500)

        # Run the script with default pixel mode but only disp interpolate
        exitCode = subprocess.call(
            [
                "spam-deformImage",
                "-disp",
                "-pf",
                "snow-ref-snow-def-orig-registration-passed-ns4.tsv",
                "snow-ref.tif",
                "-rst",
                "1",
                "-nn",
                "4",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        imDefPixDisp = tifffile.imread("snow-ref-disp-def.tif")
        self.assertTrue(numpy.abs((imDefPixDisp - imDef)[8:-8, 8:-8, 8:-8]).mean() < 500)

        # Run the script with tet mode
        exitCode = subprocess.call(
            [
                "spam-deformImage",
                "-tet",
                "-pf",
                "snow-ref-snow-def-orig-registration-passed-ns4.tsv",
                "snow-ref.tif",
                "-rst",
                "1",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        imDefScript = tifffile.imread("snow-ref-tetMesh-def.tif")
        self.assertTrue(numpy.abs((imDefScript - imDef)[8:-8, 8:-8, 8:-8]).mean() < 500)

        # Run the script with tet mode and strain correction
        exitCode = subprocess.call(
            [
                "spam-deformImage",
                "-tet",
                "-pf",
                "snow-ref-snow-def-orig-registration-passed-ns4.tsv",
                "snow-ref.tif",
                "-pre",
                "snow-ref-cgs",
                "-cgs",
                "-rst",
                "1",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        # imDefScriptCGS = tifffile.imread('snow-ref-cgs-tetMesh-def.tif')
        # self.assertTrue(numpy.allclose(imDefScript, imDefScriptCGS))
        # self.assertTrue(numpy.abs((imDefScriptCGS-imDef)[10:-10, 10:-10, 10:-10]).mean() < 500)

    def test_all_help(self):
        FNULL = open(os.devnull, "w")
        exitCode = subprocess.call(["spam-reg", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-pixelSearch", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-pixelSearchPropagate", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-ldic", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-passPhiField", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-regularStrain", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-gdic", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-deformImage", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-ITKwatershed", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-filterPhiField", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-mesh", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

    # def test_grf(self):
    #     try:
    #         import rpy2
    #     except ImportError:
    #         return
    #
    #     FNULL = open(os.devnull, "w")
    #     exitCode = subprocess.call(["spam-grf"], stdout=FNULL, stderr=FNULL)
    #     self.assertEqual(exitCode, 0)

    def test_mesh(self):
        FNULL = open(os.devnull, "w")
        exitCode = subprocess.call(
            [
                "spam-mesh",
                "-pre",
                "test-mesh",
                "-cube",
                "0.1",
                "0.4",
                "0.2",
                "0.3",
                "0",
                "1.0",
                "-lc",
                "0.1",
                "-h5",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        # read hdf5 with script
        exitCode = subprocess.call(["spam-hdf-reader", "test-mesh-lc0.1.h5"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        # check what's inside the hd5 file
        with h5py.File("test-mesh-lc0.1.h5") as f:
            self.assertEqual(f["mesh-points"].shape[1], 3)
            self.assertEqual(f["mesh-connectivity"].shape[1], 4)
            self.assertEqual(min(f["mesh-connectivity"][:].ravel()), 0)
            self.assertEqual(max(f["mesh-connectivity"][:].ravel()), f["mesh-points"].shape[0] - 1)
            for i in range(3):
                self.assertAlmostEqual(numpy.max(f["mesh-points"][:], axis=0)[i], [0.4, 0.3, 1.0][i])
                self.assertAlmostEqual(numpy.min(f["mesh-points"][:], axis=0)[i], [0.1, 0.2, 0.0][i])

    def test_mesh_subdomains(self):
        open(os.devnull, "w")
        exitCode = subprocess.call(
            [
                "spam-mesh-subdomains",
                "-pre",
                "test-mesh",
                "-cube",
                "0.1",
                "0.4",
                "0.2",
                "0.3",
                "0",
                "1.0",
                "-lc1",
                "0.05",
                "-r",
                "1",
                "2",
                "3",
                "-msh",
                "-v",
                "0",
                "-vtk",
            ]
        )
        self.assertEqual(exitCode, 0)


if __name__ == "__main__":
    unittest.main()
