import unittest

import numpy
import spam.excursions
import spam.helpers


class TestAll(spam.helpers.TestSpam):
    def test_randomfields(self):
    
        # dim = 2
        rf = spam.excursions.simulateRandomField(lengths=1.0, dim=2)
        rf = spam.excursions.simulateRandomField(lengths=[1.0, 2.0], dim=2)
        try:
            rf = spam.excursions.simulateRandomField(lengths=[1.0, 2.0, 3.0], dim=2)
        except ValueError:
            pass
        
        # dim = 3
        rf = spam.excursions.simulateRandomField(lengths=1.0, dim=3)
        try:
            rf = spam.excursions.simulateRandomField(lengths=[1.0, 2.0], dim=3)
        except ValueError:
            pass
        rf = spam.excursions.simulateRandomField(lengths=[1.0, 2.0, 3.0], dim=3)
    
        # test covariance model
        covarianceModel = "Gaussian"
        covarianceParameters = {
            # "nu": 0.5,
            "len_scale": [1, 1, 1]
        }
        lengths = 100
        nNodes = 10
        nRea = 5
        rf = spam.excursions.simulateRandomField(
            lengths=lengths,
            nNodes=nNodes,
            dim=3,
            nRea=nRea,
            seed=42,
            covarianceModel=covarianceModel,
            covarianceParameters=covarianceParameters,
            vtkFile="spam_field"
        )
        
    def test_distributions(self):
        muGauss, stdGauss, lcGauss = spam.excursions.parametersLogToGauss(1, 1)
        self.assertAlmostEqual(muGauss, -0.3465735902799726)
        self.assertAlmostEqual(stdGauss, 0.8325546111576977)
        self.assertAlmostEqual(lcGauss, 0.9281901262582816)
        u = spam.excursions.fieldGaussToUniform(numpy.array([0]))
        self.assertAlmostEqual(u[0], 0.5)
        g = spam.excursions.fieldUniformToGauss(numpy.array([0.5]))
        self.assertAlmostEqual(g[0], 0.0)
        
    def test_elkc(self):
        spam.excursions.expectedMesures(0, 1, 2)
        spam.excursions.gaussianMinkowskiFunctionals(1, 1, distributionType="log", hittingSet="head")
        spam.excursions.gaussianMinkowskiFunctionals(0, 1, hittingSet="unknown")
        spam.excursions.gaussianMinkowskiFunctionals(1, 1, distributionType="unknown")
        spam.excursions.gaussianMinkowskiFunctionals(0, -1)
        spam.excursions.secondSpectralMoment(1, 1)
        spam.excursions.secondSpectralMoment(1, 1, correlationType="matern")
        spam.excursions.secondSpectralMoment(1, 1, correlationType="matern", nu=0.5)
        spam.excursions.secondSpectralMoment(1, 1, correlationType="unknown")
        spam.excursions.flag(1, 1)
        spam.excursions.flag(1, 2)
        spam.excursions.hermitePolynomial(0, 1)
        spam.excursions.ballVolume(3)
        spam.excursions.lkcCuboid(1, 2, 1)
        spam.excursions.lkcCuboid(2, 1, 1)
        spam.excursions.lkcCuboid(1, 2, [1, 1, 1])
        spam.excursions.lkcCuboid(1, 2, [1, 1])
        spam.excursions.lkcCuboid(1, 3, [1, 1, 1])
        spam.excursions.lkcCuboid(1, 4, [1, 1, 1, 1])


if __name__ == "__main__":
    spam.helpers.TestSpam.DEBUG = True
    unittest.main()
