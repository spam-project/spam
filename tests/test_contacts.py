import math
import unittest

import numpy
import spam.kalisphera
import spam.label
import spam.label.contacts as con
import spam.label.ITKwatershed as ws


def angle_gaps(vector1, vector2):
    scalar_prod = numpy.inner(vector1, vector2)
    vector_prod = numpy.cross(vector1, vector2)
    norm_vp = numpy.sqrt((vector_prod**2).sum())
    angle_gap = numpy.arctan2(norm_vp, scalar_prod)

    if angle_gap > (math.pi / 2):
        pdt_scal_conf = numpy.inner(vector1, -vector2)
        pdt_vect_conf = numpy.cross(vector1, -vector2)
        norm_pv_conf = numpy.sqrt((pdt_vect_conf**2).sum())
        angle_gap_conf = numpy.arctan2(norm_pv_conf, pdt_scal_conf)
        vector2 = -vector2
    else:
        angle_gap_conf = angle_gap

    angle_gap_deg = numpy.degrees(angle_gap_conf)

    return angle_gap_deg


class TestAll(spam.helpers.TestSpam):
    def test_fetchTwoGrains(self):
        # creating an image
        radius = 4
        distance = 0
        orientation = [0.0, 0.0, 1.0]
        norm = numpy.sqrt(orientation[0] ** 2 + orientation[1] ** 2 + orientation[2] ** 2)
        branch = orientation / norm * (radius * 2 + distance)

        cube_size = (4 * radius) + 10

        # center sphere 1
        centre1 = [
            (cube_size / 2) - 0.5 * branch[0],
            (cube_size / 2) - 0.5 * branch[1],
            (cube_size / 2) - 0.5 * branch[2],
        ]
        # center sphere 2
        centre2 = [
            (cube_size / 2) + 0.5 * branch[0],
            (cube_size / 2) + 0.5 * branch[1],
            (cube_size / 2) + 0.5 * branch[2],
        ]

        Box = numpy.zeros((cube_size, cube_size, cube_size), dtype="<f8")

        centres = [centre1, centre2]
        radii = [radius, radius]

        spam.kalisphera.makeSphere(Box, centres, radii)

        Box[numpy.where(Box > 1.0)] = 1.0
        Box[numpy.where(Box < 0.0)] = 0.0

        # segmentation
        # binarisation
        bin_vol = (Box > 0.5) * 1
        volLab = ws.watershed(bin_vol)

        # to test
        labels = [1, 2]
        subset = con.fetchTwoGrains(volLab, labels, Box)

        self.assertEqual(numpy.unique(subset["subVolLab"]).sum(), 3)

    def test_contactingLabelsAndPoints(self):
        box = numpy.zeros((50, 50, 100), dtype="<f8")
        spam.kalisphera.makeSphere(box, [[25, 25, 50 - 20], [25, 25, 50 + 20]], [20, 20])
        box = box > 0.5
        lab = spam.label.watershed(box)

        # Multi-grain with areas
        contactingLabels, areas = spam.label.contactingLabels(lab, areas=True)
        self.assertEqual(contactingLabels[0], [0])
        self.assertEqual(contactingLabels[1], [2])
        self.assertEqual(contactingLabels[2], [1])
        self.assertEqual(areas[1], areas[2])
        self.assertEqual(areas[1][0] > 1, True)

        # Multi-grain without areas
        contactingLabels = spam.label.contactingLabels(lab, areas=False)
        self.assertEqual(contactingLabels[0], [0])
        self.assertEqual(contactingLabels[1], [2])
        self.assertEqual(contactingLabels[2], [1])

        # Test asking for a single grain with areas
        contactingLabels, areas = spam.label.contactingLabels(lab, 1, areas=True)
        self.assertEqual(contactingLabels, [2])
        self.assertEqual(areas[0] > 1, True)

        # Test asking for a single grain
        contactingLabels = spam.label.contactingLabels(lab, 1, areas=False)
        self.assertEqual(contactingLabels, [2])

        # Now test position of contacts
        conPoint = spam.label.contactPoints(lab, [1, 2])[1]
        self.assertEqual(conPoint.tolist(), [25.0, 25.0, 50.5])

        conVol = spam.label.contactPoints(lab, [1, 2], returnContactZones=True, verbose = True)
        conPoint = spam.label.centresOfMass(conVol)[1]
        self.assertEqual(conPoint.tolist(), [25.0, 25.0, 50.5])

        # GP: Adding test for the case of grain in the edge
        vol = spam.kalisphera.makeBlurryNoisySphere([20, 20, 50], [[10, 10, 0], [10, 10, 18]], [10, 8])
        lab = spam.label.watershed(vol > 0.5)
        contactingLabels, areas = spam.label.contactingLabels(lab, [1], areas=True)
        self.assertEqual(contactingLabels, [2])
        self.assertEqual(areas[0] > 1, True)

    def test_localThresholding(self):
        # test for the individual function
        # test for the function on the assembly
        # creating an image
        radius = 4
        distance = 0.3
        orientation = [0.0, 0.0, 1.0]
        norm = numpy.sqrt(orientation[0] ** 2 + orientation[1] ** 2 + orientation[2] ** 2)
        branch = orientation / norm * (radius * 2 + distance)

        cube_size = (4 * radius) + 10

        # center sphere 1
        centre1 = [
            (cube_size / 2) - 0.5 * branch[0],
            (cube_size / 2) - 0.5 * branch[1],
            (cube_size / 2) - 0.5 * branch[2],
        ]
        # center sphere 2
        centre2 = [
            (cube_size / 2) + 0.5 * branch[0],
            (cube_size / 2) + 0.5 * branch[1],
            (cube_size / 2) + 0.5 * branch[2],
        ]

        Box = numpy.zeros((cube_size, cube_size, cube_size), dtype="<f8")

        centres = [centre1, centre2]
        radii = [radius, radius]

        spam.kalisphera.makeSphere(Box, centres, radii)

        Box[numpy.where(Box > 1.0)] = 1.0
        Box[numpy.where(Box < 0.0)] = 0.0
        Box[13, 5, 13] = 0.8
        radiusThresh = 1

        localThreshold = 0.6

        CONTACT = con.localDetection(Box, localThreshold, radiusThresh)
        self.assertTrue(CONTACT)

    def test_localThresholdingAssembly(self):
        # test for the function on the assembly
        # creating an image
        radius = 4
        distance = 0.3
        orientation = [0.0, 0.0, 1.0]
        norm = numpy.sqrt(orientation[0] ** 2 + orientation[1] ** 2 + orientation[2] ** 2)
        branch_1 = orientation / norm * (radius * 2 + 0.0)
        branch_2 = orientation / norm * (radius * 2 + distance)

        # -------------
        # case 1
        # cube_size = ( 4 * radius ) + 10

        # center sphere 1
        # centre1 = [(cube_size/2)-0.5*branch[0], (cube_size/2)-0.5*branch[1], (cube_size/2)-0.5*branch[2]]
        # center sphere 2
        # centre2 =  [(cube_size/2)+0.5*branch[0], (cube_size/2)+0.5*branch[1], (cube_size/2)+0.5*branch[2]]

        # Box = numpy.zeros( (cube_size, cube_size, cube_size), dtype="<f8")

        # centres = [centre1, centre2]
        # radii = [radius, radius]

        # -------------
        # case 2
        # image of 3 spheres in contact to check the more statements of the code
        cube_size = (6 * radius) + 10

        # centers
        centre1 = [
            (cube_size / 2) - 1.0 * branch_1[0],
            (cube_size / 2) - 1.0 * branch_1[1],
            (cube_size / 2) - 1.0 * branch_1[2],
        ]
        centre2 = [
            (cube_size / 2) + 0.0 * branch_1[0],
            (cube_size / 2) + 0.0 * branch_1[1],
            (cube_size / 2) + 0.0 * branch_1[2],
        ]
        centre3 = [
            (cube_size / 2) + 1.0 * branch_2[0],
            (cube_size / 2) + 1.0 * branch_2[1],
            (cube_size / 2) + 1.0 * branch_2[2],
        ]

        Box = numpy.zeros((cube_size, cube_size, cube_size), dtype="<f8")

        centres = [centre1, centre2, centre3]
        radii = [radius, radius, radius]

        spam.kalisphera.makeSphere(Box, centres, radii)

        Box[numpy.where(Box > 1.0)] = 1.0
        Box[numpy.where(Box < 0.0)] = 0.0

        # segmentation
        # binarisation
        bin_vol = (Box > 0.5) * 1
        volLab = ws.watershed(bin_vol)

        localThreshold = 0.7

        contactVolume, Z, contactsTable, contactingLabels = con.labelledContacts(volLab)
        # check whether we found two apparent contacts
        self.assertEqual(numpy.shape(contactingLabels)[0], 2)

        contactListRefined = con.localDetectionAssembly(volLab, Box, contactingLabels, localThreshold)
        # if not contactListRefined:
        # print("List is empty")
        # check whether we refined and excluded one contact (imposed distance)
        self.assertEqual(numpy.shape(contactListRefined)[0], 1)

    def test_orientationDetermination(self):
        # creating an image
        radius = 10
        distance = 0
        orientation = [0.0, 1.0, 1.0]
        norm = numpy.sqrt(orientation[0] ** 2 + orientation[1] ** 2 + orientation[2] ** 2)
        branch = orientation / norm * (radius * 2 + distance)

        cube_size = (4 * radius) + 10

        # center sphere 1
        centre1 = [
            (cube_size / 2) - 0.5 * branch[0],
            (cube_size / 2) - 0.5 * branch[1],
            (cube_size / 2) - 0.5 * branch[2],
        ]
        # center sphere 2
        centre2 = [
            (cube_size / 2) + 0.5 * branch[0],
            (cube_size / 2) + 0.5 * branch[1],
            (cube_size / 2) + 0.5 * branch[2],
        ]

        Box = numpy.zeros((cube_size, cube_size, cube_size), dtype="<f8")

        centres = [centre1, centre2]
        radii = [radius, radius]

        spam.kalisphera.makeSphere(Box, centres, radii)

        Box[numpy.where(Box > 1.0)] = 1.0
        Box[numpy.where(Box < 0.0)] = 0.0

        # segmentation
        # binarisation
        bin_vol = (Box > 0.5) * 1
        volLab = ws.watershed(bin_vol)

        contactVolume, Z, contactsTable, contactingLabels = con.labelledContacts(volLab)

        contactNormal, intervox, NotTreatedContact = con.contactOrientations(bin_vol, volLab, "ITK")

        angle_gap_deg = angle_gaps(orientation, contactNormal)
        self.assertTrue(angle_gap_deg < 0.5)
        contactNormal, intervox, NotTreatedContact = con.contactOrientations(bin_vol, volLab, "RW")

        angle_gap_deg = angle_gaps(orientation, contactNormal)
        self.assertTrue(angle_gap_deg < 0.5)

    def test_orientationAssembly(self):
        # creating an image
        radius = 5
        # case 1
        distance = 0
        orientation = [0.0, 0.0, 1.0]
        norm = numpy.sqrt(orientation[0] ** 2 + orientation[1] ** 2 + orientation[2] ** 2)
        branch = orientation / norm * (radius * 2 + distance)

        # case 1
        # cube_size = ( 4 * radius ) + 10

        # center sphere 1
        # centre1 = [(cube_size/2)-0.5*branch[0], (cube_size/2)-0.5*branch[1], (cube_size/2)-0.5*branch[2]]
        # centre2 = [(cube_size/2)+0.5*branch[0], (cube_size/2)+0.5*branch[1], (cube_size/2)+0.5*branch[2]]

        # Box = numpy.zeros( (cube_size, cube_size, cube_size), dtype="<f8")

        # centres = [centre1, centre2]
        # radii = [radius, radius]

        # spam.kalisphera.makeSphere(Box, centres, radii)

        # case 2
        # image of 3 spheres in contact to check the more statements of the code
        cube_size = (6 * radius) + 10

        # centers
        centre1 = [
            (cube_size / 2) - 1.0 * branch[0],
            (cube_size / 2) - 1.0 * branch[1],
            (cube_size / 2) - 1.0 * branch[2],
        ]
        centre2 = [
            (cube_size / 2) + 0.0 * branch[0],
            (cube_size / 2) + 0.0 * branch[1],
            (cube_size / 2) + 0.0 * branch[2],
        ]
        centre3 = [
            (cube_size / 2) + 1.0 * branch[0],
            (cube_size / 2) + 1.0 * branch[1],
            (cube_size / 2) + 1.0 * branch[2],
        ]

        Box = numpy.zeros((cube_size, cube_size, cube_size), dtype="<f8")

        centres = [centre1, centre2, centre3]
        radii = [radius, radius, radius]

        spam.kalisphera.makeSphere(Box, centres, radii)

        Box[numpy.where(Box > 1.0)] = 1.0
        Box[numpy.where(Box < 0.0)] = 0.0

        # segmentation
        # binarisation
        bin_vol = (Box > 0.5) * 1
        volLab = ws.watershed(bin_vol)

        contactVolume, Z, contactsTable, contactingLabels = con.labelledContacts(volLab)

        watershed = "ITK"
        contactOrientations = con.contactOrientationsAssembly(volLab, Box, contactingLabels, watershed)

        # test whether the computed orientations are close to the imposed ones
        angle_gap_deg = angle_gaps(orientation, contactOrientations[0, 2:5])
        self.assertTrue(angle_gap_deg < 0.5)

        angle_gap_deg = angle_gaps(orientation, contactOrientations[1, 2:5])
        self.assertTrue(angle_gap_deg < 0.5)

    def test_markerCorrection(self):
        # case 1
        markers = numpy.zeros((3, 3, 3), dtype="u1")
        markers[1, 1, 1] = 1
        numMarkers = 1
        edm = numpy.copy(markers)
        volBin = numpy.copy(markers)
        marker = con._markerCorrection(markers, numMarkers, edm, volBin)

        self.assertTrue(numpy.unique(marker).sum() == 0)

        # case 2
        markers = numpy.zeros((3, 5, 5))
        markers[1, 1, 1] = 1
        markers[1, 3, 2] = 2
        markers[1, 2, 3] = 3
        numMarkers = 3
        edm = (markers > 0) * 1
        volBin = numpy.copy(edm)
        marker = con._markerCorrection(markers, numMarkers, edm, volBin)

        self.assertTrue(numpy.unique(marker).sum() == 3)


if __name__ == "__main__":
    unittest.main()
