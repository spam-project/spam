#!/bin/bash
set -e -x

export PIP_ROOT_USER_ACTION=ignore

PYBINS=(
#   "/opt/python/cp27-cp27m/bin"  # Gone from manylinux2014
#   "/opt/python/cp27-cp27mu/bin" # Gone from manylinux2014
#    "/opt/python/cp35-cp35m/bin"
#    "/opt/python/cp36-cp36m/bin"
#    "/opt/python/cp37-cp37m/bin"
#    "/opt/python/cp38-cp38/bin"
   "/opt/python/cp39-cp39/bin"
   "/opt/python/cp310-cp310/bin"
   "/opt/python/cp311-cp311/bin"
   "/opt/python/cp312-cp312/bin"
  )


# Compile wheels
for PYBIN in ${PYBINS[@]}; do
    # New! Let's install into venvs, this allows spam- scripts to
    # be installed correctly and thus to pass the tests if we need
    # to do that
    pyVers=`"${PYBIN}/python" --version  | awk '{print $2}'`
    if [ -d venv-$pyVers ]; then
      echo "Looks like venv already exists"
    else
      "${PYBIN}/python" -m venv venv-$pyVers
    fi
    source venv-$pyVers/bin/activate
    pip install -U pip
    pip install -U wheel
    pip install -U setuptools
    pip install .
#     pip install pytest pytest-xdist
#     pytest -n auto
    pip wheel -w wheelhouse/ .
    rm -rf build dist
    deactivate
done


# Bundle external shared libraries into the wheels
for whl in wheelhouse/spam*.whl; do
    auditwheel repair "$whl" --plat manylinux2014_x86_64 -w /wheelhouse/
done
