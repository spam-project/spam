.. _DIC_examples:

Digital Image Correlation examples
-----------------------------------

This is a set of examples for the use of the basic image transformation
and image correlation functions
